const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './src/index.html',    
    filename: 'index.html',
    inject: 'body'
});


module.exports = {
    entry: './src/index.js',
    output: {
      path: path.resolve('dist'),
      filename: 'all.js' 
    },
    resolve: { modules: ["node_modules"] },
    module: {
        loaders :[
	    { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/},
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/}
        ]
    },
    plugins: [HtmlWebpackPluginConfig]

}
